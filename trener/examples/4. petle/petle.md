```js

while(true){ /* infinite loop */}

do{} while()

for( var i; i< 10; i++){
    continue
    break
}

for( var i in array){  item = array[i]  }
for( var key in obj){  item = obj[key]  }

// ES6 

for(let index in array){ 
    item = array[index] 

    // array[index + 1] = costam
}

for(let item of array){  item  /* no index */ }

```
