```js


sum = (...numbers) => {
  return numbers.reduce((s, n) => s + n);
};


sum(1, 2, 3, 4, 5);
// 15;

tablica = [1, 2, 3, 4, 5](5)[(1, 2, 3, 4, 5)];
sum(...tablica);
// 15;

function a() {
  console.log(Array.from(arguments));
}
```

# ES5

```js

function a(/* ....arguments */) {
  console.log(Array.prototype.slice.apply(arguments));
}

```
