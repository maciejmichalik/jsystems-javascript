
# Destructuring 
```js

person = {
    name:'Alice', 
    address: { street: 'Sezamkowa', city:'NY' },
    company: { name:"ACME" },
    results: [100,80,95]
}

function getInfo(person){
    var {
        name, 
        address: { street, city },
        company: { name: companyName },
        results: [firstScore,,lastScore]
    } = person

    return `${name} - [${companyName}] - ${street},${city} - ${lastScore} points`
}
getInfo(person)
// 'Alice - [ACME] - Sezamkowa,NY - 95 points'

```

# Destructuring with =default and ...spread
```js
function getInfo(person){
    var {
        name, 
        address: { street, city },
        company: { name: companyName },
        team: { name:teamName } = { name:'No Team' },
        results: [firstScore, ...lastScores]
    } = person

return `${name} ${teamName} - [${companyName}] - ${street},${city} - ${lastScores} points`
}
getInfo(person)
// 'Alice No Team - [ACME] - Sezamkowa,NY - 80,95 points'
person.team = {name:'Rockets'}
// {name: 'Rockets'}
getInfo(person)
// 'Alice Rockets - [ACME] - Sezamkowa,NY - 80,95 points'
```

```js

getInfo = ({
    name, 
    address: { street, city },
    company: { name: companyName },
    team: { name:teamName } = { name:'No Team' },
    results: [firstScore, ...lastScores]
}) =>  
  `${name} ${teamName} - [${companyName}] 
  - ${street},${city} - ${lastScores} points`;

getInfo(person)
// 'Alice Rockets - [ACME] \n    - Sezamkowa,NY - 80,95 points'

```