```js


window
// Window {window: Window, self: Window, document: document, name: '', location: Location, …}

window.document
// #document

window.document.head
// <head>​…​</head>

window.document.body
// <body>​…​</body>​

window.document.head.childNodes
// NodeList(13) [text, meta, text, meta, text, meta, text, title, text, link, text, script, text]

window.document.head.children
// HTMLCollection(6) [meta, meta, meta, title, link, script, viewport: meta]

window.document.body.children
// HTMLCollection [div.container]

window.document.body.children[0].children[0].children[0].children
// HTMLCollection(3) [h1, dl, table.table.table-striped]

```
