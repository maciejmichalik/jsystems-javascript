
## AMD


```js
// https://requirejs.org/docs/whyamd.html#purposes
// https://requirejs.org/docs/optimization.html#onejs

// ./index.html
// ./jquery.js

require('./jquery', function($){
    //  $....
});

```

<!-- https://jcubic.pl/2017/07/uniwersalne-biblioteki-javascript.html -->