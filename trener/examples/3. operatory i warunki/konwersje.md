```js

(80.34974999999999)
// 80.34974999999999

(80.34974999999999).toPrecision(2)
// '80'

(180.34974999999999).toPrecision(2)
// '1.8e+2'

// 1.8e+2 = 1.8 x 102 = 180

(180.34974999999999).toFixed(2)
// '180.35'

(180.34474999999999).toFixed(2)
// '180.34'

Math.round(1.5)
// 2
Math.round(1.4)
// 1
Math.ceil(1.2)
// 2
Math.floor(1.9)
// 1


```
## IEEE 754
<!-- https://pl.wikipedia.org/wiki/IEEE_754 -->

```js

0.1 + 0.1
// 0.2
0.1 + 0.3
// 0.4
0.1 + 0.2
// 0.30000000000000004

```

## BigInt
```js
100_000_000n
100000000n
100_000_000n + 100 
// VM19241:1 Uncaught TypeError: Cannot mix BigInt and other types, use explicit conversions
//     at <anonymous>:1:14
// (anonymous) @ VM19241:1
100_000_000n / 10 
// VM19273:1 Uncaught TypeError: Cannot mix BigInt and other types, use explicit conversions
//     at <anonymous>:1:14
// (anonymous) @ VM19273:1
100_000_000n / 10n 
// 10000000n
100_000_000n / 333333333n 
// 0n
100_000_000n / 333n 
// 300300n
```

## Type conversion
```js
"37" + 7
// '377'
37 - 7
// 30

'37' + {} 
// '37[object Object]'
({}).toString()
// '[object Object]'
'37' + new Date()
// '37Mon Feb 14 2022 14:41:05 GMT+0100 (Central European Standard Time)'
parseInt("37") + 7
// 44
parseInt("37 kg ziemniakow") + 7
// 44
parseInt("placki i 37 kg ziemniakow") + 7
// NaN
parseInt("37 kg ziemniakow i 2 placki") + 7
// 44

```

## Error Handling
```js
100 / 0 
// Infinity
0 / 0 
// NaN
{} - 8
// -8
[] - 2
// -2
NaN + NaN - NaN / NaN 
// NaN
NaN == NaN
// false
isFinite(100/0)
// false
isNaN(0/0)
// true
```
