```js
function echo(msg, fakeErr) {
  return new Promise((resolve, reject) => {
    setTimeout(function () {
      fakeErr ? reject(fakeErr) : resolve(msg);
    }, 2000);
  });
}

p = echo("Ala");
p2 = p.then((res) => {
  return echo(res + " ma ");
});
p3 = p2.then((res) => echo(res + " kota "));
p3.then(console.log);
// Promise {<pending>}

/// 6sec...
// Ala ma  kota
```

```js
// ...
p3 = p2.then((res) => echo(res + " kota "));
p3.then(console.log);

p3b = p2.then((res) => echo(res + " psa "));
p3b.then(console.log);
// Promise {<pending>}
// Ala ma  kota
// Ala ma  psa
```

## Error handling

```js
function echo(msg, fakeErr){
    return new Promise((resolve, reject) => {
        setTimeout(function(){
            fakeErr? reject(fakeErr) : resolve(msg)
        },2000)
    })
}

p = echo('Ala', 'ups.. ')
p2 = p
    // .catch( err => { return 'Anonymous' })
    .then( res => { return echo(res + ' ma ') } )
p3 = p2.then( res => echo(res + ' kota ') )
p3
.then(console.log)
.catch( err => { console.error(err) })
// Promise {<pending>}
// VM6077:16 ups.. 
```

## Promise helpers

```js
Promise.resolve('test').then(console.log)
// test
// Promise {<fulfilled>: undefined}

Promise.reject('upss').catch(console.log)
console.log('sync')
// VM6435:2 sync
// upss

Promise.all([echo('A'), echo('B').then(()=>echo('B2'))]).then(console.log) 
// Promise {<pending>}
// (2) ['A', 'B2']
```