## Callbacks

```js
function echo(msg, callbackFn){
    setTimeout(function(){
        callbackFn(msg)
    },2000)
}

// res = echo('Ala ma kota') // Sync/Blocking operation

// Async - not blocking
echo('Ala ma kota', function(res){
    console.log(res)
});

console.log("najpierw to")
// najpierw to

/// .... 2 sec ....
//  Ala ma kota

```

## Nested callbacks

```js

function echo(msg, callbackFn){
    setTimeout(function(){
        callbackFn(msg)
    },2000)
}

echo('Ala', function(res){
    echo(res+' ma ', function(res){
         echo(res+' kota ', function(res){
            console.log('asynchroniczne',res)
        });
    });
});
console.log('synchroniczne')
// synchroniczne

// .. 6 sec..
// asynchroniczne Ala ma  kota 
```
