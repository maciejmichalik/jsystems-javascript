```js

// Products
// - use template to create array of products
// - add same reviews to each product
// - each product should have different id, name, price

var product = {
    id: '',
    name: "",
    description: "",
    price: 0,
    promotion: false,
    reviews: [ { rating: 5 }]
}

product1 = {};
for(var klucz in product){
    product1[klucz] = product[klucz]
}
product1.name = 'Jabłka'
// product1.id = 'Jabłka'
// product1.description = 'Jabłka'
// product1.price = 'Jabłka'

product2 = product;
product2.name = 'Gruszki'

product3 = product;
product3.name = 'Placki'

product3.reviews.push('ocena')
product1.reviews
// (2) [{…}, 'ocena']

var products = [product1,product2,product3]

// product['ala ma kota 😋'] = '😋'

console.log(product)



```