```js

owoc = 'jablko'
// 'jablko'
drzewoA = {}
drzewoB = {}
// {}owoc: "jablko"[[Prototype]]: Object
drzewoA.owoc = owoc
drzewoB.owoc = owoc
// 'jablko'
owoc = 'gruszka'
// 'gruszka'
owoc 
// 'gruszka'
drzewoA
// {owoc: 'jablko'}
drzewoA.owoc = 'gruszka'
// 'gruszka'
drzewoA
// {owoc: 'gruszka'}
drzewoB 
// {owoc: 'jablko'}

```

```js

owoc = 'jablko'
// 'jablko'
galaz = { owoc: owoc }
// {owoc: 'jablko'}
drzewoA.galaz = galaz
drzewoB.galaz = galaz
// {owoc: 'jablko'}
owoc = 'gruszka'
// 'gruszka'
drzewoA.galaz
// {owoc: 'jablko'}
galaz.owoc = 'banan'
// 'banan'
drzewoA.galaz
// {owoc: 'banan'}
drzewoB.galaz
// {owoc: 'banan'}
drzewoA.galaz.owoc = 'ananas'
// 'ananas'
drzewoB.galaz.owoc 
// 'ananas'

```