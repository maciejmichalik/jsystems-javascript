```js

        // A:
        function doAllSteps(){
            step1 = func1()
            step2 = func2(step1)
            step3 = func3(step3)
        }

        // B:
        function func1(){ func2() //...
        function func2(){ func3() //...
        function func3(){ //...

```
