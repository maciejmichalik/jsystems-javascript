node -v 
v16.13.1

npm -v
6.14.6

git --version
git version 2.31.1.windows.1

Google Chrome	97.0.4692.99 

# git 

https://bitbucket.org/ev45ive/jsystems-javascript
logujemy się -> klikamy po lewej (+) -> Fork this repository

Na waszym forku klikamy Clone > wybieramy HTTPS i kopiujemy polecenie:
git clone https://bitbucket.org/<TUTAJ_WASZA_NAZWA>/jsystems-javascript.git jsystems-javascript

cd jsystems-javascript

git remote add trener  https://bitbucket.org/ev45ive/jsystems-javascript


git config --global user.name "Mateusz Kulesza"
git config --global user.email "Mateusz@Kulesza"

<!-- 
[remote "origin"]
      fetch = +refs/heads/*:refs/remotes/trener/* -->

## GIT Update:

-> git push origin master
<- git pull trener master
## Javascript
https://en.wikipedia.org/wiki/Self_(programming_language)
https://pl.wikipedia.org/wiki/Scheme

## HTML5 Web APIs
https://whatwebcando.today/

## No Cache
Devtools (F12) -> Network -> Disable Cache (while Devtools open)