import { products } from "./products.js";


class CartConstuctor {

    _cartData = {
        items: [
            {
                id: '999',
                name: 'Product ABC',
                price: 1000,
                amount: 2,
                subtotal: 2000,
            }
        ],
        total: 2000
    }

    _recalculateTotal(item) {
        item.subtotal = item.price * item.amount;
        this._cartData.total = this._cartData.items.reduce(function (total, item) {
            return total + item.subtotal
        }, 0)
    }

    findItem(id) {
        return this._cartData.items.find(p => p.id === id)
    }

    add(product) {
        let item = this.findItem(product.id)
        if (item) {
            item.amount++
        } else {
            item = {
                id: product.id,
                name: product.name,
                price: product.price,
                amount: 1,
            }
            this._cartData.items.push(item)
        }
        this._recalculateTotal(item)
    }

    remove(product) {
        let item = this.findItem(product.id)
        if (item) {
            item.amount--
        }
        this._cartData.items = this._cartData.items.filter(function (item) { return item.amount > 0 })
        this._recalculateTotal(item)
    }

    getTotal() {
        return this._cartData.total;
    }

    getItems() {
        return this._cartData.items;
    }
}


window.cart = new CartConstuctor()

// =================

console.assert(cart.getTotal() === 2000, 'Bad total')
console.assert(cart.getItems().length == 1, 'Bad items')
console.assert(cart.findItem('999').subtotal == 2000, 'Bad Subtotal')

cart.add(products[0])
console.assert(cart.getItems().length == 2, 'Bad items')
console.assert(cart.getTotal() === 11750, 'Bad total')

cart.add(products[0])
console.assert(cart.getItems().length == 2, 'Bad items')
console.assert(cart.findItem(products[0].id).subtotal == 19500, 'Bad subtotal')
console.assert(cart.getTotal() === 21500, 'Bad total')

cart.remove({ id: '999' })
console.assert(cart.getItems().length == 2, 'Bad items')
console.assert(cart.getTotal() === 20500, 'Bad total')
cart.remove({ id: '999' })
console.assert(cart.getItems().length == 1, 'Bad items')
console.assert(cart.getTotal() === 19500, 'Bad total')



/* 
function CartConstuctor2() {

    const _cartData = {
        items: [
            {
                id: '999',
                name: 'Product ABC',
                price: 1000,
                amount: 2,
                subtotal: 2000,
            }
        ],
        total: 2000
    }

    const self = ({
        _cartData: _cartData,
        _recalculateTotal: _recalculateTotal,
        findItem: findCartItem,
        add: addToCart,
        remove: removeFromCart,
        getTotal: getCartTotal,
        getItems: getCartItems,
    })
    return self

    function _recalculateTotal(item) {
        item.subtotal = item.price * item.amount;
        self._cartData.total = _cartData.items.reduce(function (total, item) {
            return total + item.subtotal
        }, 0)
    }

    function findCartItem(id) {
        return self._cartData.items.find(p => p.id === id)
    }

    function addToCart(product) {
        let item = self.findItem(product.id)
        if (item) {
            item.amount++
        } else {
            item = {
                id: product.id,
                name: product.name,
                price: product.price,
                amount: 1,
            }
            self._cartData.items.push(item)
        }
        self._recalculateTotal(item)
    }

    function removeFromCart(product) {
        let item = self.findItem(product.id)
        if (item) {
            item.amount--
        }
        self._cartData.items = self._cartData.items.filter(function (item) { return item.amount > 0 })
        self._recalculateTotal(item)
    }

    function getCartTotal() {
        return self._cartData.total;
    }

    function getCartItems() {
        return self._cartData.items;
    }

}
 */// =================