import { ProductsListComponent } from './src/ProductsListComponent.js';
import { SloganGenerator } from './src/SloganGenerator.js';

// Import as ...
import * as ProductItems from './src/ProductListItem.js';
// Default export:
import productsData from './src/products.js';

export class Main {
  constructor() {

    this.component = new ProductsListComponent(productsData);
    this.component.isPromoted = true;
    this.component.limit = 2;
    // this.component.renderItem = ProductItems.ProductListItem
    this.component.renderItem = ProductItems.ProductListItemHoliday;
    this.component.sloganGenerator = new SloganGenerator();
  }
  start() {
    this.component.render();
  }
}
