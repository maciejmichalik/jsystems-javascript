console.log('HElo widgets');

const tabelaElem = document.getElementById("tabela_raty");


// const { rows, totals } = updateTable()
// console.table(rows)
// console.table(totals)

const params = {
  // Kwota kredytu
  total: 1000_00,
  // Oprocentowanie roczne
  interest: 0.15,
  // Ilosc miesiecy
  periods: 12
}

const totalEl = document.getElementById('total')
totalEl.value = params.total / 100
totalEl.addEventListener('input', function (event) {
  params.total = this.value * 100
})

document.getElementById('interest').value = params.interest * 100
document.getElementById('interest_output').innerText = `${params.interest * 100} %`
document.getElementById('interest').addEventListener('input', (event) => {
  params.interest = event.currentTarget.value
  document.getElementById('interest_output').innerText = `${params.interest * 100} %`
})

document.getElementById('periods').value = params.periods
document.getElementById('periods_output').innerText = `${params.periods} miesiecy`
document.getElementById('periods').addEventListener('input', (event) => {
  params.periods = event.currentTarget.value
  document.getElementById('periods_output').innerText = `${params.periods} miesiecy`
})

function calculate(event) {
  console.log(event, this)
  updateTable()
}

const calculateBtn = document.getElementById('calculateBtn')
calculateBtn.onclick = calculate

function updateTable() {
  clearTable()
  const rows = []
  const totals = {
    payments: 0,
    capital: 0,
    interest: 0,
  }
  let remining = params.total;
  for (let i = 0; i < params.periods; i++) {
    const capital = params.total / params.periods;
    const interest = remining * params.interest / params.periods
    const payment = capital + interest
    const row = {
      period: i + 1,
      remining,
      payment,
      capital,
      interest
    }
    rows.push(row)
    totals.payments += payment
    totals.capital += capital
    totals.interest += interest
    remining -= capital
    generateRow(row);
  }
  generateFooterRow(totals)
  return { rows, totals }
}


function formatCurrency(cents) {
  return (cents / 100).toFixed(2) + ' zł'
}

function generateRow(row) {
  let rowEl = document.createElement('tr');
  // https://marketplace.visualstudio.com/items?itemName=Tobermory.es6-string-html
  rowEl.innerHTML = /* html */`
    <td>${row.period}</td>
    <td>${formatCurrency(row.remining)}</td>
    <td>${formatCurrency(row.payment)}</td>
    <td>${formatCurrency(row.capital)}</td>
    <td>${formatCurrency(row.interest)}</td>
  `
  tabelaElem.querySelector('tbody').append(rowEl);
}

function generateFooterRow(totals) {
  let rowEl = document.createElement('tr');
  // https://marketplace.visualstudio.com/items?itemName=Tobermory.es6-string-html
  rowEl.innerHTML = /* html */`
    <th colspan="2">Suma:</th>
    <th>${formatCurrency(totals.payments)}</th>
    <th>${formatCurrency(totals.capital)}</th>
    <th>${formatCurrency(totals.interest)}</th>
  `
  tabelaElem.querySelector('tfoot').append(rowEl);
}

function clearTable() {
  // while(tabelaElem.querySelector('tbody').firstElementChild)
  //       tabelaElem.querySelector('tbody').firstElementChild.remove()
  tabelaElem.querySelector('tbody').innerHTML = ''
  tabelaElem.querySelector('tfoot').innerHTML = ''
}